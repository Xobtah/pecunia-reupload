# pecunia-reupload

This is a reupload, the original repository contained credentials.

Simple cryptocurrency trading bot. The bot is run every minute by a CRON job, it checks whether it has to buy, sell, or do nothing based on the current price and highest observed price.  It has not been implemented to trade real cryptocurrency, it is only a simulation.

The trading algorithm is designed by my brother.

Improvements :
* Integrate ELK stash to log the bot's activity

## Build

> $ cargo build --release

## Configure CRON

> $ crontab -e
> * * * * * RUST_LOG=info /usr/local/bin/pecunia /etc/opt/pecunia/pecunia.json